/* THE TEAM */
  Company: Accedo Techonology
  Website: http://bpoinsight.com
  Contact: info@bpoinsight.com

  Project Manager: Ivan Bendana
  Contact: ivan.bendana@accedotech.com
  Social Media: [linkedin, facebook, twitter]

  Lead Developer: Gershell López
  Contact: gershell.lopez@accedotech.com
  Social Media: [linkedin, facebook, twitter]

  Web Designer: Gershell López
  Contact: gershell.lopez@accedotech.com
  Social Media: [linkedin, facebook, twitter]

/* THANKS */
  Extension Provider: [insert name]
  Website: [company url]
  Contact: [email/contact info]

  Client Liason: [insert name]
  Website: [company url]
  Contact: [email/contact info]

/* SITE DETAILS */
  Company Name
  URL
  Launch Date
  Date of Latest Update