<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $post
 */
?>
<nav class="large-3 medium-4 columns nav-actions">
    <ul class="nav nav-pills">
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $post->id],
                ['confirm' => __('Esta seguro que desea borrar el # {0}?', $post->id),'class'=>'btn btn-danger']
            )
        ?></li>
        <li class="nav-item"><?= $this->Html->link(__('Lista de Posts'), ['action' => 'index'],['class'=>'btn btn-info tn btn-info']) ?></li>
        <li class="nav-item"><?= $this->Html->link(__('Lista de Categorías'), ['controller' => 'Categories', 'action' => 'index'],['class'=>'btn btn-info tn btn-info']) ?></li>
        <li class="nav-item"><?= $this->Html->link(__('Nueva Categoría'), ['controller' => 'Categories', 'action' => 'add'],['class'=>'btn btn-info tn btn-info']) ?></li>
    </ul>
</nav>

<div class="card">
    <?= $this->Form->create($post,['type' => 'file']) ?>
    <div class="card-header">
        <strong>Editar</strong>
        <small>
            Post
        </small>
    </div>
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?= $this->Form->control('name',['class'=>'form-control']); ?>
                </div>    
                <div class="col-md-6">
                    <?= $this->Form->control('category_id', ['options' => $categories,'class'=>'form-control']); ?>
                </div>    
                <div class="col-md-12">
                    <?= $this->Form->control('description',['class'=>'form-control tinymce']); ?>
                </div>    
                <div class="col-md-12">
                    <?= $this->Form->control('author',['class'=>'form-control']); ?>
                </div>    
                <div class="col-md-12">
                    <?= $this->Form->control('intro',['class'=>'form-control']); ?>
                </div>  
                <div class="col-md-6">
                    <?= $this->Form->control('tags',['class'=>'form-control','options'=>$tags,'multiple'=>true, 'value'=>$tagPostData]); ?>
                </div>    
                <div class="col-md-6">
                    <?= $this->Form->control('caption_image',['class'=>'form-control']); ?>
                </div>   
                <div class="col-md-6">
                    <?= $this->Form->control('meta_title',['class'=>'form-control']); ?>
                    <?= $this->Form->control('meta_description',['class'=>'form-control']); ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('facebook_title',['class'=>'form-control']); ?>
                    <?= $this->Form->control('facebook_description',['class'=>'form-control']); ?>
                </div> 
                <div class="col-md-6">
                    <?= $this->Form->control('twitter_title',['class'=>'form-control']); ?>
                    <?= $this->Form->control('twitter_description',['class'=>'form-control']); ?>
                </div>    
                <div class="col-md-6">
                    <?= $this->Form->control('photo',['class'=>'form-control','type'=>'file']); ?>
                    <?= $this->Html->image('/files/posts/photo/' . $post->photo_dir . '/' . $post->photo,['class'=>'img-fluid']); ?>
                </div>    
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= $this->Form->button('Submit',['class'=>'btn btn-success']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>

