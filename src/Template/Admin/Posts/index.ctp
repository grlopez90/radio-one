<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post[]|\Cake\Collection\CollectionInterface $posts
 */
?>
<nav class="large-3 medium-4 columns nav-actions">
    <ul class="nav nav-pills">
        <li class="nav-item"><?= $this->Html->link(__('Nuevo Post'), ['action' => 'add'],['class'=>'nav-link tn btn-info']) ?></li>
        <li class="nav-item"><?= $this->Html->link(__('Categorías'), ['controller' => 'Categories', 'action' => 'index'],['class'=>'nav-link tn btn-info']) ?></li>
        <li class="nav-item"><?= $this->Html->link(__('Nueva Categoría'), ['controller' => 'Categories', 'action' => 'add'],['class'=>'nav-link tn btn-info']) ?></li>
    </ul>
</nav>
<div class="card">
    <div class="card-header">
        <strong>Posts</strong>
        <small>Lista</small>
    </div>
    <div class="card-body">
      <div class="posts index large-9 medium-8 columns content">
        <table cellpadding="0" cellspacing="0" class="table">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('permalink') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('category_id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('tags') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                    <th scope="col" class="actions"><?= __('Acciones') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($posts as $post): ?>
                <tr>
                    <td><?= $this->Number->format($post->id) ?></td>
                    <td><?= h($post->name) ?></td>
                    <td><?= h($post->permalink) ?></td>
                    <td><?= $post->has('category') ? $this->Html->link($post->category->name, ['controller' => 'Categories', 'action' => 'view', $post->category->id]) : '' ?></td>
                    <td><?= h($post->tags) ?></td>
                    <td><?= h($post->created) ?></td>
                    <td><?= h($post->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Editar'), ['action' => 'edit', $post->id],['class'=>'btn btn-primary btn-sm']) ?>
                        <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $post->id], ['confirm' => __('Esta seguro que desea eliminiar el # {0}?', $post->id),'class'=>'btn btn-danger btn-sm']) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('Primero')) ?>
                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
                <?= $this->Paginator->last(__('Último') . ' >>') ?>
            </ul>
            <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, Mostrando {{current}} de {{count}} en total')]) ?></p>
        </div>
    </div>
</div>