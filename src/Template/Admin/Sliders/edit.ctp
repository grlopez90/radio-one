<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Slider $slider
 */
?>
<nav class="large-3 medium-4 columns nav-actions">
    <ul class="nav nav-pills">
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $post->id],
                ['confirm' => __('Esta seguro que desea borrar el # {0}?', $slider->id),'class'=>'btn btn-danger']
            )
        ?></li>
    </ul>
</nav>
<div class="card">
    <?= $this->Form->create($slider,['type' => 'file']) ?>
    <div class="card-header">
        <strong>Agregar</strong>
        <small>
            Post
        </small>
    </div>
    <?= $this->Form->create($slider) ?>
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?php echo $this->Form->control('name',['class'=>'form-control']); ?>
                </div>
                <div class="col-md-6">
                    <?php echo $this->Form->control('photo',['class'=>'form-control','type'=>'file']); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= $this->Form->button('Submit',['class'=>'btn btn-success']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>

