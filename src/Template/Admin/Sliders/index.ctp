<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Slider[]|\Cake\Collection\CollectionInterface $sliders
 */
?>
<nav class="large-3 medium-4 columns nav-actions">
    <ul class="nav nav-pills">
        <li class="nav-item"><?= $this->Html->link(__('Nuevo Slide'), ['action' => 'add'],['class'=>'nav-link tn btn-info']) ?></li>
    </ul>
</nav>
<div class="card">
    <div class="card-header">
        <strong>Slider</strong>
        <small>Lista</small>
    </div>
    <div class="card-body">
        <div class="sliders index large-9 medium-8 columns content">
            <h3><?= __('Sliders') ?></h3>
            <table cellpadding="0" cellspacing="0" class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('photo_dir') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('photo') ?></th>
                        <th scope="col" class="actions"><?= __('Actions') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($sliders as $slider): ?>
                    <tr>
                        <td><?= $this->Number->format($slider->id) ?></td>
                        <td><?= h($slider->name) ?></td>
                        <td><?= h($slider->created) ?></td>
                        <td><?= h($slider->modified) ?></td>
                        <td><?= h($slider->photo_dir) ?></td>
                        <td><?= h($slider->photo) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $slider->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $slider->id]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $slider->id], ['confirm' => __('Are you sure you want to delete # {0}?', $slider->id)]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('first')) ?>
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                    <?= $this->Paginator->last(__('last') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
        </div>
    </div>
</div>
