<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tag $tag
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar Tag'), ['action' => 'edit', $tag->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar Tag'), ['action' => 'delete', $tag->id], ['confirm' => __('Esta seguro que desea borrar el # {0}?', $tag->id)]) ?> </li>
        <li><?= $this->Html->link(__('Lista de Tags'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo Tag'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="tags view large-9 medium-8 columns content">
    <h3><?= h($tag->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($tag->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Permalink') ?></th>
            <td><?= h($tag->permalink) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Meta Title') ?></th>
            <td><?= h($tag->meta_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Facebook Title') ?></th>
            <td><?= h($tag->facebook_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Twitter Title') ?></th>
            <td><?= h($tag->twitter_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($tag->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($tag->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($tag->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Meta Description') ?></h4>
        <?= $this->Text->autoParagraph(h($tag->meta_description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Facebook Description') ?></h4>
        <?= $this->Text->autoParagraph(h($tag->facebook_description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Twitter Description') ?></h4>
        <?= $this->Text->autoParagraph(h($tag->twitter_description)); ?>
    </div>
</div>
