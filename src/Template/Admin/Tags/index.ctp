<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Tag[]|\Cake\Collection\CollectionInterface $tags
 */
?>
<nav class="large-3 medium-4 columns nav-actions">
    <ul class="nav nav-pills">
        <li class="nav-item"><?= $this->Html->link(__('Lista de Posts'), ['controller' => 'posts','action' => 'index'],['class'=>'nav-link tn btn-info']) ?></li>
        <li class="nav-item"><?= $this->Html->link(__('Nuevo Tag'), ['controller' => 'tags', 'action' => 'add'],['class'=>'nav-link tn btn-info']) ?></li>
    </ul>
</nav>
<div class="card">
    <div class="card-header">
        <strong>Tags</strong>
        <small>Lista</small>
    </div>
    <div class="card-body">
        <div class="tags index large-9 medium-8 columns content">
            <table cellpadding="0" cellspacing="0" class="table">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('permalink') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                        <th scope="col" class="actions"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($tags as $tag): ?>
                    <tr>
                        <td><?= $this->Number->format($tag->id) ?></td>
                        <td><?= h($tag->name) ?></td>
                        <td><?= h($tag->permalink) ?></td>
                        <td><?= h($tag->created) ?></td>
                        <td><?= h($tag->modified) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Editar'), ['action' => 'edit', $tag->id], ['class'=>'btn btn-primary btn-sm']) ?>
                            <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $tag->id], ['confirm' => __('Esta seguro que desea borrar el # {0}?', $tag->id),'class'=>'btn btn-danger btn-sm']) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->first('<< ' . __('Primero')) ?>
                    <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
                    <?= $this->Paginator->last(__('{Último}') . ' >>') ?>
                </ul>
                <p><?= $this->Paginator->counter(['format' => __('Página {{page}} de {{pages}}, Mostrando {{current}} de {{count}} en total')]) ?></p>
        </div>
    </div>
</div>