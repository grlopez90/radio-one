<div class="sidebar">
    <nav class="sidebar-nav">
        <ul class="nav">
            <li class="nav-title">
                Modules
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="<?= $this->Url->build('/admin/posts'); ?>">
                    <i class="icon-puzzle"></i> Posts
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->Url->build('/admin/sliders'); ?>"><i class="icon-calculator"></i> Sliders</a>
            </li>
            <li class="nav-item nav-dropdown">
                <a class="nav-link" href="<?= $this->Url->build('/admin/categories'); ?>"><i class="icon-star"></i> Categorías</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->Url->build('/admin/tags'); ?>"><i class="icon-calculator"></i> Tags</a>
            </li>
        </ul>
    </nav>
    <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>