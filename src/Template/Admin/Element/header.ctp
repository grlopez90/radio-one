<button class="navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<a class="navbar-brand" href="#"></a>
<button class="navbar-toggler sidebar-toggler d-md-down-none" type="button">
<span class="navbar-toggler-icon"></span>
</button>
<ul class="nav navbar-nav ml-auto account-dropdown">   
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <img src="img/avatars/6.jpg" class="img-avatar" alt="admin@bootstrapmaster.com">
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            <div class="dropdown-header text-center">
                <strong>Usuario</strong>
            </div>            
            <a class="dropdown-item" href="#"><i class="fa fa-lock"></i> Cerrar Sesión</a>
        </div>
    </li>
</ul>
