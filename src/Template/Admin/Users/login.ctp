<!--
    * CoreUI - Open Source Bootstrap Admin Template
    * @version v1.0.4
    * @link http://coreui.io
    * Copyright (c) 2017 creativeLabs Łukasz Holeczek
    * @license MIT
    -->
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- <link rel="shortcut icon" href="assets/ico/favicon.png"> -->
        <title>Radio One Login</title>
        <?= $this->Html->css('admin'); ?>
    </head>
    <body class="app flex-row align-items-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card-group">
                        <div class="card p-4">
                            <div class="card-body">
                                <?= $this->Flash->render() ?>
                                <h1>Login</h1>
                                <p class="text-muted">Inicie sesión en su cuenta</p>
                                <?= $this->Form->create(null, [
                                    'url' => ['controller' => 'users', 'action' => 'login'],
                                    'type' => 'post'
                                ]); 
                                $this->Form->templates([
                                    'inputContainer' => '{{content}}'
                                ]);
                                ?>
                                <div class="input-group mb-3">
                                    <span class="input-group-addon"><i class="icon-user"></i></span>
                                    <?= $this->Form->control('username',['class'=>'form-control','label'=>false,'placeholder'=>'Nombre de Usuario']); ?>
                                </div>
                                <div class="input-group mb-4">
                                    <span class="input-group-addon"><i class="icon-lock"></i></span>
                                    <?= $this->Form->control('password',['class'=>'form-control','label'=>false,'placeholder'=>'Contraseña']); ?>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <button type="submit" class="btn btn-primary px-4">Iniciar Sesión</button>
                                    </div>
                                    <div class="col-6 text-right">
                                        <button type="button" class="btn btn-link px-0">Olvidó su contraseña?</button>
                                    </div>
                                </div>
                                <?= $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Bootstrap and necessary plugins -->
        <script src="node_modules/jquery/dist/jquery.min.js"></script>
        <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    </body>
</html>