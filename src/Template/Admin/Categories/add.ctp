<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<nav class="large-3 medium-4 columns nav-actions" id="actions-sidebar">
    <ul class="nav nav-pills">
        <li class="nav-item"><?= $this->Html->link(__('Lista de Categorías'), ['action' => 'index'],['class'=>'nav-link btn btn-info']) ?></li>
        <li class="nav-item"><?= $this->Html->link(__('Lista de Posts'), ['controller' => 'Posts', 'action' => 'index'],['class'=>'nav-link btn btn-info']) ?></li>
        <li class="nav-item"><?= $this->Html->link(__('Nuevo Post'), ['controller' => 'Posts', 'action' => 'add'],['class'=>'nav-link btn btn-info']) ?></li>
    </ul>
</nav>
<div class="card">
    <?= $this->Form->create($category) ?>
    <div class="card-header">
        <strong>Agregar</strong>
        <small>
            Categoría
        </small>
    </div>
    <div class="card-body">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->control('name',['class'=>'form-control']); ?>
                </div>    
                
                <div class="col-md-6">
                    <?= $this->Form->control('meta_title',['class'=>'form-control']); ?>
                    <?= $this->Form->control('meta_description',['class'=>'form-control']); ?>
                </div>
                <div class="col-md-6">
                    <?= $this->Form->control('facebook_title',['class'=>'form-control']); ?>
                    <?= $this->Form->control('facebook_description',['class'=>'form-control']); ?>
                </div> 
                <div class="col-md-6">
                    <?= $this->Form->control('twitter_title',['class'=>'form-control']); ?>
                    <?= $this->Form->control('twitter_description',['class'=>'form-control']); ?>
                </div> 
            </div>
        </div>
    </div>
    <div class="card-footer">
        <?= $this->Form->button('Submit',['class'=>'btn btn-success']) ?>
    </div>
    <?= $this->Form->end() ?>
</div>
