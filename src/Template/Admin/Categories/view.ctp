<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Acciones') ?></li>
        <li><?= $this->Html->link(__('Editar Categoría'), ['action' => 'edit', $category->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Eliminar Categoría'), ['action' => 'delete', $category->id], ['confirm' => __('Esta seguro que desea borrar el # {0}?', $category->id)]) ?> </li>
        <li><?= $this->Html->link(__('Lista de Categorías'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nueva Categoría'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('Lista de Posts'), ['controller' => 'Posts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nuevo Post'), ['controller' => 'Posts', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="categories view large-9 medium-8 columns content">
    <h3><?= h($category->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($category->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Permanlink') ?></th>
            <td><?= h($category->permanlink) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Meta Title') ?></th>
            <td><?= h($category->meta_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Facebook Title') ?></th>
            <td><?= h($category->facebook_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Twitter Title') ?></th>
            <td><?= h($category->twitter_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($category->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($category->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($category->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Meta Description') ?></h4>
        <?= $this->Text->autoParagraph(h($category->meta_description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Facebook Description') ?></h4>
        <?= $this->Text->autoParagraph(h($category->facebook_description)); ?>
    </div>
    <div class="row">
        <h4><?= __('Twitter Description') ?></h4>
        <?= $this->Text->autoParagraph(h($category->twitter_description)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Posts') ?></h4>
        <?php if (!empty($category->posts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Permanlink') ?></th>
                <th scope="col"><?= __('Description') ?></th>
                <th scope="col"><?= __('Category Id') ?></th>
                <th scope="col"><?= __('Tags') ?></th>
                <th scope="col"><?= __('Caption Image') ?></th>
                <th scope="col"><?= __('Meta Title') ?></th>
                <th scope="col"><?= __('Meta Description') ?></th>
                <th scope="col"><?= __('Facebook Title') ?></th>
                <th scope="col"><?= __('Facebook Description') ?></th>
                <th scope="col"><?= __('Twitter Title') ?></th>
                <th scope="col"><?= __('Twitter Description') ?></th>
                <th scope="col"><?= __('Photo Dir') ?></th>
                <th scope="col"><?= __('Photo') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Acciones') ?></th>
            </tr>
            <?php foreach ($category->posts as $posts): ?>
            <tr>
                <td><?= h($posts->id) ?></td>
                <td><?= h($posts->name) ?></td>
                <td><?= h($posts->permanlink) ?></td>
                <td><?= h($posts->description) ?></td>
                <td><?= h($posts->category_id) ?></td>
                <td><?= h($posts->tags) ?></td>
                <td><?= h($posts->caption_image) ?></td>
                <td><?= h($posts->meta_title) ?></td>
                <td><?= h($posts->meta_description) ?></td>
                <td><?= h($posts->facebook_title) ?></td>
                <td><?= h($posts->facebook_description) ?></td>
                <td><?= h($posts->twitter_title) ?></td>
                <td><?= h($posts->twitter_description) ?></td>
                <td><?= h($posts->photo_dir) ?></td>
                <td><?= h($posts->photo) ?></td>
                <td><?= h($posts->created) ?></td>
                <td><?= h($posts->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['controller' => 'Posts', 'action' => 'view', $posts->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['controller' => 'Posts', 'action' => 'edit', $posts->id]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['controller' => 'Posts', 'action' => 'delete', $posts->id], ['confirm' => __('Esta seguro que desea borrar el # {0}?', $posts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
