<main class="ContactUs">
    <div class="container">
        <h2 class="ContactUs-title radio-one-title-logo">Contactanos</h2>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="ContactUs-contentWrapper">
                    <p class="ContactUs-content">
                        Somos una emisora diseñada para darte lo mejor de la música e información de los 365 días del año, con una programación variada y muy ágil, dándote siempre los mejores éxitos de tus artistas favoritos!
                    </p>
                    <?= $this->Html->image('contactUs-mic.png', ['alt' => 'Stereo Headset', 'class' => 'ContactUs-headset']) ?>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <form action="" method="post" id='ContactForm' class="ContactUs-form">
                    <div class="form-row">
                        <div class="col-12">
                            <input type="text" name="ContactName" id="ContactName" class="form-control ContactUs-nameInput" placeholder="nombre completo">
                        </div>
                        <div class="col-12">
                            <input type="email" name="ContactEmail" id="ContactEmail" class="form-control ContactUs-emailInput" placeholder="correo electrónico">
                        </div>
                        <div class="col-12">
                            <textarea name="ContactMessage" id="ContactMessage" cols="30" rows="10" class="form-control ContactUs-messageInput" placeholder="mensaje"></textarea>
                        </div>
                        <div class="col-12 col-md-8">
                            <p class="ContactUs-formNote">
                                Gracias por comunicarte con nosotros. Un representante de Radio ONE se comunicará con usted lo antes posible.
                            </p>
                        </div>
                        <div class="col-12 col-md-4">
                            <button type="submit" class="btn btn-primary ContactUs-formSubmit">enviar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>