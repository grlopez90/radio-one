<main class="Article">
    <div class="container">
        <h2 class="Article-sectionTitle radio-one-title-logo">Articulos</h2>
        <?= $this->Html->image("/files/posts/photo/{$post->photo_dir}/{$post->photo}", ['alt' => 'Post', 'class' => 'Article-banner']); ?>
        
        <div class="Article-wrapper radio-one-bg-white">
            <span class="Article-date"><?= $post->created ?></span>
            <h1 class="Article-title">
                <?= $post->name ?>
            </h1>
            <p class="Article-content">
                <?= $post->description ?>
            </p>
        </div>
    </div>
</main>
<aside class="RelatedPosts">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <article class="PostItem">                    
                    <?= $this->Html->image('post-img.jpg', ['alt' => 'Post', 'class' => 'PostItem-image']); ?>

                    <div class="PostItem-content">
                        <h3 class="PostItem-title">Lorem Ipsum dolor sit amet consectetur</h3>
                        <span class="PostItem-date">11/12/2018</span>
                        <p class="PostItem-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, dolorum rem rerum libero ipsa recusandae, blanditiis iusto repudiandae odit temporibus accusamus quod esse provident impedit odio dolores cumque consequatur quibusdam...</p>
                        <a href="/view/1" class="PostItem-readMore">Leer Mas</a>
                    </div>
                </article>
            </div>
            <div class="col-12 col-md-6">
                <article class="PostItem">                    
                    <?= $this->Html->image('post-img.jpg', ['alt' => 'Post', 'class' => 'PostItem-image']); ?>

                    <div class="PostItem-content">
                        <h3 class="PostItem-title">Lorem Ipsum dolor sit amet consectetur</h3>
                        <span class="PostItem-date">11/12/2018</span>
                        <p class="PostItem-description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat, dolorum rem rerum libero ipsa recusandae, blanditiis iusto repudiandae odit temporibus accusamus quod esse provident impedit odio dolores cumque consequatur quibusdam...</p>
                        <a href="/view/1" class="PostItem-readMore">Leer Mas</a>
                    </div>
                </article>
            </div>
        </div>
    </div>
</aside>