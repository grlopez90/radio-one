<section class="MainSection">
    <div class="MainSection-firstSlide">  
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="/img/banner.png" alt="First slide">
                    <div class="MainSection-content container">
                        <div class="MainSection-contentPlay">
                            <a href="" class="Play Player" data-task="play">
                                <i class="fas fa-play"></i>
                            </a>
                            <audio controls autoplay id="Audio" class="AudioPlayer">
                                <source src="http://18.224.110.145:8000/;?type=http&nocache=15" type="audio/mpeg">
                                Your browser does not support the audio element.
                            </audio>
                        </div>
                        <div class="MainSection-contentTitles">
                            <span class="MainSection-contentTitle-2">
                                RadioStreaming...
                            </span>
                            <span class="MainSection-contentTitle-1">
                                SOLO UNO SABE LO QUE LE GUSTA...
                            </span>
                        </div>
                    </div>
                </div>
                <?php foreach($sliders as $slide): ?>
                <div class="carousel-item">
                    <?= $this->Html->image("/files/sliders/photo/{$slide->photo_dir}/{$slide->photo}",['class' => 'd-block w-100']); ?>
                    <div class="MainSection-content container">
                        <div class="MainSection-contentTitles">
                            <?php if (!empty($slide->name)): ?>
                            <span class="MainSection-contentTitle-1">
                                <?= $slide->name ?>
                            </span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>