<div class="container">
	<div class="row">
		<div class="col-md-12 col-lg-6">
			<div class="Logo">
				<a href="" title="RadioOne.com">
					<img src="/img/logo.png" class="Logo-full" alt="">
				</a>
			</div>
		</div>
		<div class="col-md-12 col-lg-6">
			<div class="Menu">
				<div class="Menu-content">
					<ul class="Menu-list">
						<li class="Menu-item <?=$selected=='home'?'selected':''?>">
							<a class="Menu-link <?=$selected=='home'?'selected':''?>" href="/home">INICIO</a>
						</li>
						<li class="Menu-item <?=$selected=='posts'?'selected':''?>">
							<a class="Menu-link <?=$selected=='posts'?'selected':''?>" href="/posts">ARTÍCULOS</a>
						</li>
						<li class="Menu-item <?=$selected=='contact'?'selected':''?>">
							<a class="Menu-link <?=$selected=='contact'?'selected':''?>" href="/contact">CONTACTANOS</a>
						</li>
						<li class="Menu-item Player">
							<a href="" class="Player Menu-player" data-task="play">
								<i class="fas fa-volume-off" ></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
