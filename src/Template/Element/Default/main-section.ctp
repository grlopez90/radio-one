
<section class="MainSection" style="background: url('https://accedotechnologies.com/img/background/bg-home.jpg') #CCC; background-position: center">
    <div class="white-overlay">
        <div class="container">
            <div class="Header-plate">
                <div class="Header-plateContent">
                    <div class="Header-plateScrew top-left"></div>
                    <div class="Header-plateScrew top-right"></div>
                    <div class="Header-plateScrew bot-left"></div>
                    <div class="Header-plateScrew bot-right"></div>

                    <h1 class="Header-plateTitle">Este es el título del Primer <span>POST</span></h1>
                </div>
            </div>
        </div>
    </div>
</section>