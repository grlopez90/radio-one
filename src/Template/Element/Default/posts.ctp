<div class="row">
    <?php foreach($posts as $post):?>
    <div class="col-12 col-md-6">
        <article class="PostItem">
            <?= $this->Html->image("/files/posts/photo/{$post->photo_dir}/{$post->photo}", ['alt' => 'Post', 'class' => 'PostItem-image']); ?>
            <div class="PostItem-content">
                <h3 class="PostItem-title"><?= $post->name ?></h3>
                <span class="PostItem-date"><?= $post->created ?></span>
                <p class="PostItem-description"><?= $this->Text->truncate($post->description); ?></p>
                <a href="/posts/view/<?= $post->id . '/' . $post->permalink  ?>" class="PostItem-readMore">Leer Mas</a>
            </div>
        </article>
    </div>
    <?php endforeach;?>
</div>
