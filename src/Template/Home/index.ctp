<?= $this->element("/Default/slide"); ?>

<section class="Posts HomePosts">
    <div class="container">
        <h2 class="Posts-title radio-one-title-logo">Lo Nuevo</h2>
        <?= $this->element('Default/posts'); ?>
    </div>
</section>

<section class="Ad">
    <div class="container">
        <div class="Ad-wrapper">
            <?= $this->Html->image('radio-one-ad.jpg', ['alt' => 'Anunciate en Radio One', 'class' => 'Ad-image']) ?>
            <div class="Ad-info">
                <div class="Ad-infoWrapper">
                    <h3 class="Ad-title">Anunciate en <span class="radio-one-sec-color">Radio One 92.9FM</span></h3>
                    <ul class="Ad-offers">
                        <li class="Ad-offersItem">Pautas de cuña rotativas</li>
                        <li class="Ad-offersItem">Transmisiones en vivo</li>
                        <li class="Ad-offersItem">Menciones</li>
                        <li class="Ad-offersItem">Cortinas de apertura y cierre</li>
                    </ul>
                    <span class="Ad-note">Podemos hacerle paquetes según sus necesidades</span>
                    <a href="#" class="Ad-quote">Cotizalo</a>
                </div>
            </div>
        </div>
        
    </div>
</section>

<!-- <aside class="SocialCols">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.2&appId=1475918959143468&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
                <div class="fb-page" data-href="https://www.facebook.com/Emisoras-de-Panama-187085278029443/" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/Emisoras-de-Panama-187085278029443/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Emisoras-de-Panama-187085278029443/">Emisoras de Panama</a></blockquote></div>
            
            </div>
            <div class="col-md-6">
                <a class="twitter-timeline" href="https://twitter.com/radiosdepanama?ref_src=twsrc%5Etfw">Tweets by radiosdepanama</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
            </div>
        </div>
    </div>
</aside> -->
