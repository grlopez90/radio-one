<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html lang="es" xml:lang="es">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <title>
        <?= @$title_for_layout; ?>
    </title>
    <?php if (!empty($description_for_layout)): ?>
    <meta name="description" content="<?= @$description_for_layout ?>">
    <?php endif ?>

    <?php if (!empty($keywords)): ?>
	<meta name="keywords" content="<?= @$keywords ?>">
    <?php endif ?>

    <?= $this->Html->meta('favicon.png','img/favicon.png',['type' => 'icon']); ?>
    
    <?php if (!empty($facebookMetas)): ?>
	<meta charset="UTF-8">
	<meta property="og:type" content="article">
	<meta property="og:site_name" content="bpoinsight">
    <meta property="og:url" content="<?= $this->Url->build($post->category->permalink.'/'.$post->permalink,true) ?>">
    <meta property="og:title" content="<?= @$facebookMetas['site_name'] ?>">
	<meta property="og:description" content="<?= @$facebookMetas['description'] ?>">
	<meta property="og:image" content="<?= $this->Url->build(@$facebookMetas['image'],true) ?>">
	<?php endif ?>
	<?php if (!empty($twitterMetas)): ?>
	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@bpoinsight" />
	<meta name="twitter:title" content="<?= @$twitterMetas['site_name'] ?>" />
	<meta name="twitter:description" content="<?= @$twitterMetas['description'] ?>" />
	<meta name="twitter:image" content="<?= $this->Url->build(@$twitterMetas['image'],true) ?>" />
	<?php endif ?>

    <link rel="stylesheet" href="<?php echo $this->Elixir->version('css/default.css'); ?>">

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <header class="Header">
        <?= $this->element('Default/header'); ?>
    </header>
    <div id="content" class="MainContent">
        <?= $this->fetch('content') ?>
    </div>
    <footer class="Footer">
        <?= $this->element('Default/footer'); ?>
    </footer>
    <?php $this->TinymceElfinder->defineElfinderBrowser()?>
    <?= $this->Html->script('all.js'); ?>
    <script>
        $(document).ready(function(){
            $("#Audio").trigger('pause');
            $(".Player").on("click", function(event) {
                event.preventDefault();
                var task = $(this).attr('data-task');
                if(task == 'play'){
                    $("#Audio").trigger('load');
                    $(".Play").attr('data-task','stop');
                    $(".Menu-player").attr('data-task','stop');
                    $(".Play").children().attr('class','fas fa-pause');
                    $(".Menu-player").children().attr('class','fas fa-volume-up');
                }
                if(task == 'stop'){
                    $("#Audio").trigger('pause');
                    $(".Play").attr('data-task','play');
                    $(".Menu-player").attr('data-task','play');
                    $(".Play").children().attr('class','fas fa-play');
                    $(".Menu-player").children().attr('class','fas fa-volume-off');
                }
            }); 

        });
    </script>
</body>
</html>
