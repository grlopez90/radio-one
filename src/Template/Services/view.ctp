<section class="LayoutContent">
	<section class="Service">
        <div class="black-overlay Service-wrapper">
            <h2 class="Service-title">
                Title of this Service
            </h2>
        </div>
    </section>
    <section class="ServiceDetail">
        <div class="container">
            <div class="ServiceDetail-content">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti placeat cum ex praesentium! Non maiores autem exercitationem, sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. Sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. </p>\
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti placeat cum ex praesentium! Non maiores autem exercitationem, sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. Sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. </p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti placeat cum ex praesentium! Non maiores autem exercitationem, sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. Sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. </p>
            </div>
        </div>
    </section>
</section>