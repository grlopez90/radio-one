<section class="LayoutContent">
	<section class="AboutUs">
        <div class="black-overlay">
            <div class="container">
                <h2 class="Services-title">
                    Nuestros Servicios
                </h2>
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <a href="/services/1" class="Services-box-link">
                            <div class="Services-box">
                                <h3 class="Services-box-title">
                                    Servicio Numero 1
                                </h3>
                                <p class="Services-box-content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti placeat cum ex praesentium! Non maiores autem exercitationem, sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. Sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi.
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <a href="/services/1" class="Services-box-link">
                            <div class="Services-box">
                                <h3 class="Services-box-title">
                                    Servicio Numero 1
                                </h3>
                                <p class="Services-box-content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti placeat cum ex praesentium! Non maiores autem exercitationem, sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. Sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi.
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <a href="/services/1" class="Services-box-link">
                            <div class="Services-box">
                                <h3 class="Services-box-title">
                                    Servicio Numero 1
                                </h3>
                                <p class="Services-box-content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti placeat cum ex praesentium! Non maiores autem exercitationem, sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. Sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi.
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <a href="/services/1" class="Services-box-link">
                            <div class="Services-box">
                                <h3 class="Services-box-title">
                                    Servicio Numero 1
                                </h3>
                                <p class="Services-box-content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti placeat cum ex praesentium! Non maiores autem exercitationem, sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. Sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi.
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <a href="/services/1" class="Services-box-link">
                            <div class="Services-box">
                                <h3 class="Services-box-title">
                                    Servicio Numero 1
                                </h3>
                                <p class="Services-box-content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti placeat cum ex praesentium! Non maiores autem exercitationem, sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. Sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi.
                                </p>
                            </div>
                        </a>
                    </div>
                    <div class="col-xs-12 col-md-6 col-lg-4">
                        <a href="/services/1" class="Services-box-link">
                            <div class="Services-box">
                                <h3 class="Services-box-title">
                                    Servicio Numero 1
                                </h3>
                                <p class="Services-box-content">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti placeat cum ex praesentium! Non maiores autem exercitationem, sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi. Sunt dolores voluptatibus hic possimus animi. Itaque illo, expedita placeat minima porro excepturi.
                                </p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>          
    <section class="Contact">
        <div class="container">
            <div class="Contact-wrapper">
                <span class="Contact-title">
                    Contacto.
                </span>
                <p class="Contact-address">
                    Dirección: Del supermercado 1C al sur 20 abajo, continúe recto casa color de ejemplo
                </p>
                <form action="PostContact" method="POST">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <input type="text" class="form-control Contact-nameInput" id="ContactName" placeholder="Nombre">
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <input type="email" class="form-control Contact-emailInput" id="ContactEmail" placeholder="Correo">
                        </div>
                    </div>                    
                    
                    <textarea class="form-control Contact-messageInput" id="ContactMessage" placeholder="Mensaje"></textarea>                       
                    <button type="submit" class="btn btn-default Contact-send">Enviar</button>
                </form>
            </div>
        </div>
    </section>
</section>