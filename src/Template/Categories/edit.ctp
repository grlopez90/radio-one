<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Category $category
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Eliminar'),
                ['action' => 'delete', $category->id],
                ['confirm' => __('Esta seguro que desea borrar el # {0}?', $category->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('Lista de Categorías'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Lista de Posts'), ['controller' => 'Posts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('Nuevo Post'), ['controller' => 'Posts', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="categories form large-9 medium-8 columns content">
    <?= $this->Form->create($category) ?>
    <fieldset>
        <legend><?= __('Edit Category') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('permanlink');
            echo $this->Form->control('meta_title');
            echo $this->Form->control('meta_description');
            echo $this->Form->control('facebook_title');
            echo $this->Form->control('facebook_description');
            echo $this->Form->control('twitter_title');
            echo $this->Form->control('twitter_description');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
