<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Category Entity
 *
 * @property int $id
 * @property string $name
 * @property string $permanlink
 * @property string $meta_title
 * @property string $meta_description
 * @property string $facebook_title
 * @property string $facebook_description
 * @property string $twitter_title
 * @property string $twitter_description
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Post[] $posts
 */
class Category extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'permalink' => true,
        'meta_title' => true,
        'meta_description' => true,
        'facebook_title' => true,
        'facebook_description' => true,
        'twitter_title' => true,
        'twitter_description' => true,
        'created' => true,
        'modified' => true,
        'posts' => true
    ];
}
