<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Post Entity
 *
 * @property int $id
 * @property string $name
 * @property string $permalink
 * @property string $description
 * @property string $category_id
 * @property string $tags
 * @property string $caption_image
 * @property string $meta_title
 * @property string $meta_description
 * @property string $facebook_title
 * @property string $facebook_description
 * @property string $twitter_title
 * @property string $twitter_description
 * @property string $photo_dir
 * @property $photo
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Category $category
 */
class Post extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'permalink' => true,
        'description' => true,
        'author' => true,
        'intro' => true,
        'category_id' => true,
        'tags' => true,
        'caption_image' => true,
        'meta_title' => true,
        'meta_description' => true,
        'facebook_title' => true,
        'facebook_description' => true,
        'twitter_title' => true,
        'twitter_description' => true,
        'photo_dir' => false,
        'photo' => true,
        'created' => true,
        'modified' => true,
        'category' => true
    ];
}
