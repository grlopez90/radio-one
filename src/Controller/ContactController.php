<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Contact Controller
 *
 *
 * @method \App\Model\Entity\Contact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContactController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->set(['title_for_layout' => 'Contacto - Radio One 92.9']);
        $this->set('selected','contact');
    }   
    
    public function beforeFilter(Event $event){
        parent::beforeFilter($event);

        $this->Auth->allow(['index']);
    }

}
