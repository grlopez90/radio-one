<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

/**
 * Home Controller
 *
 *
 * @method \App\Model\Entity\Home[] paginate($object = null, array $settings = [])
 */
class HomeController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->loadModel('Posts');
        $this->loadModel('Sliders');

        $options = [
            'order' => ['Posts.created' => 'DESC']
        ];
        $posts = $this->Posts->find('all',$options);

        $sliders = $this->Sliders->find('all');

        $description_for_layout = 'Radio One 92.9 fm en Bocas del Toro.';
        $keywords = 'Radio,92.9';

        $this->set(['description_for_layout' => $description_for_layout, 'keywords' => $keywords, 'title_for_layout' => 'Radio One 92.9']);
        $this->set('selected','home');
        $this->set(compact('posts'));
        $this->set(compact('sliders'));
    }

    /**
     * View method
     *
     * @param string|null $id Home id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $home = $this->Home->get($id, [
            'contain' => []
        ]);

        $this->set('home', $home);
        $this->set('_serialize', ['home']);
    }

    
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['index', 'logout']);
    }

}
