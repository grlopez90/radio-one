<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[] paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index($category = null)
    {
        $this->paginate = [
            'order' => ['Posts.created' => 'DESC']
        ];
        $posts = $this->paginate($this->Posts);
        $this->loadModel('Sliders');

        $sliders = $this->Sliders->find('all');

        $this->set(compact('posts'));
        $this->set('_serialize', ['posts']);
        $this->set(['title_for_layout' => 'Posts - Radio One 92.9']);
        $this->set('selected','posts');
        $this->set(compact('sliders'));
    }

    /**
     * Populars method
     *
     * @return \Cake\Http\Response|void
     */
    public function populars() {

        $posts = $this->Posts->find()
            ->order(['Posts.views'=>'DESC'])
            ->contain(['Categories']);

        $this->set(compact('posts'));
        $this->set('_serialize', ['posts']);   
    }

    /**
     * Lastest method
     *
     * @return \Cake\Http\Response|void
     */
    public function lastest() {

        $posts = $this->Posts->find()
            ->order(['Posts.created'=>'DESC'])
            ->contain(['Categories']);

        $this->set(compact('posts'));
        $this->set('_serialize', ['posts']);   
    }

    /**
     * View method
     *
     * @param string|null $category Post category.
     * @param string|null $slug Post permalink.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null, $slug = null)
    {
        $post = $this->Posts->find()
            ->where(['Posts.permalink'=>$slug,'Posts.id'=>$id])
            ->first();

        // if (!empty($post)) {
        //     $postView = TableRegistry::get('Posts');
        //     $postUpdate = $postView->get($post->id);

        //     $post->views = $post->views + 1;
        //     $postView->save($postUpdate);
        // }

        $description_for_layout = $post->meta_description;
        $keywords = $post->tags;

        $facebookMetas['site_name'] = $post->name;
        $facebookMetas['description'] = $post->meta_description;
        $facebookMetas['image'] = "/files/posts/photo/{$post->photo_dir}/{$post->photo}";

        $twitterMetas['site_name'] = $post->name;
        $twitterMetas['description'] = $post->meta_description;
        $twitterMetas['image'] = "/files/posts/photo/{$post->photo_dir}/{$post->photo}";

        $this->set([
            'description_for_layout' => $description_for_layout, 
            'keywords' => $keywords,
            'facebookMetas' => $facebookMetas,
            'twitterMetas' => $twitterMetas
        ]);

        $this->set('post', $post);
        $this->set('_serialize', ['post']);
        $this->set('selected','posts');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function tags($slug = null)
    {
        $this->paginate = [
            'contain' => ['Categories'],
            'conditions' => ['Posts.tags LIKE' => "%{$slug}%"],
            'order' => ['Posts.created' => 'DESC']
        ];
        $posts = $this->paginate($this->Posts);

        $this->set(compact('posts'));
        $this->render('index');
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow(['index','view','tags','populars','lastest']);
    }
}
