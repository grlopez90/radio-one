<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Utility\Text;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[] paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // $this->paginate = [
        //     'contain' => ['Categories']
        // ];
        $posts = $this->paginate($this->Posts);

        $this->set(compact('posts'));
        $this->set('_serialize', ['posts']);
    }

    /**
     * View method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => ['Categories']
        ]);

        $this->set('post', $post);
        $this->set('_serialize', ['post']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());            
            if ($this->Posts->save($post)) {
                $this->Flash->success(sprintf('<div class="alert alert-success">%s</div>', h('The Post has been saved.')), ['escape' => false]);
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(sprintf('<div class="alert alert-danger">%s</div>', h('The Post could not be saved. Please, try again.')), ['escape' => false]);
        }
        $categories = $this->Posts->Categories->find('list', ['limit' => 200]);
        $this->loadModel('Tags');
        $tags = $this->Tags->find('list',[
            'keyField' => 'name',
            'valueField' => 'name'
        ]);
        $this->set(compact('post', 'categories','tags'));
        $this->set('_serialize', ['post']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            if ($this->Posts->save($post)) {
                $this->Flash->success(sprintf('<div class="alert alert-success">%s</div>', h('The Post has been saved.')), ['escape' => false]);

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(sprintf('<div class="alert alert-danger">%s</div>', h('The Post could not be saved. Please, try again.')), ['escape' => false]);
        }
        $categories = $this->Posts->Categories->find('list', ['limit' => 200]);
        $this->loadModel('Tags');
        $tags = $this->Tags->find('list',[
            'keyField' => 'name',
            'valueField' => 'name'
        ]);
        $postTags = explode(',',$post->tags);

        //esto lo hice asi solo para probar el query builder habia manera mas facil de hacerlo
        $tagsInPost = $this->Tags->find()
            ->select('Tags.name')
            ->where(function($exp, $q) use ($postTags){
                return $exp->in('Tags.name', $postTags);
            })->toArray();

        $tagPostData = [];
        foreach ($tagsInPost as $tag) {
            $tagPostData[] =  $tag->name;
        } //aqui acaba el bloque de prueba que hice

        $this->set(compact('post', 'categories','tags','tagPostData'));
        $this->set('_serialize', ['post']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Post id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $post = $this->Posts->get($id);
        if ($this->Posts->delete($post)) {
            $this->Flash->success(__('The post has been deleted.'));
        } else {
            $this->Flash->error(__('The post could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
    }
}
