const elixir = require('cakephp3-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Extensions
 |--------------------------------------------------------------------------
 |
 | Elixir provides several extensions which bring new functionality to Elixir.
 | A couple of examples have been included below to get you started.
 | You can use any extension made for the most recent version of Elixir 6.
 | Feel free to try out different extensions!
 | Please report any problems to: https://github.com/pfuri/cakephp-elixir/issues
 |
 */
//require('laravel-elixir-del');
//require('laravel-elixir-webpack-official');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir(mix => {
        // Example
		//mix.del(["webroot/build", "webroot/css", "webroot/js"])
    	//.sass('default.scss').version('css/default.css')
        //.webpack('default.js').version('js/default.js');

        mix.sass('default.scss');
});

elixir(function(mix) {
    mix.sass('admin.scss');
});

elixir(function(mix) {
    mix.copy(
    	'node_modules/chosen-npm/public/chosen.css',
    	'webroot/css'
    ).copy(
    	'node_modules/chosen-npm/public/chosen-sprite.png',
    	'webroot/css'
    ).copy(
        './node_modules/jquery/dist/jquery.js',
        'webroot/js'
    ).copy(
        'node_modules/tinymce/plugins',
        'webroot/js/plugins'
    ).copy(
        'node_modules/tinymce/skins',
        'webroot/js/skins'
    ).copy(
        'node_modules/tinymce/themes',
        'webroot/js/themes'
    );
});

elixir(function(mix) {
    mix.scripts([
        './node_modules/jquery/dist/jquery.js',
        './node_modules/popper.js/dist/umd/popper.min.js',
        './node_modules/bootstrap/dist/js/bootstrap.js',
        './node_modules/chosen-npm/public/chosen.jquery.js',
        './node_modules/tinymce/tinymce.js',
        './node_modules/vue/dist/vue.js',
        './node_modules/axios/dist/axios.js',
        'common.js',
        'tags.js',
        'posts.js'
    ]);
});

/*elixir(function(mix) {
    mix.webpack('default.js');
});*/

elixir(function(mix) {
    mix.version('css/default.css');
});
